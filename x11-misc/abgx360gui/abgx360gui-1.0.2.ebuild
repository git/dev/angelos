# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
WX_GTK_VER=2.8

inherit eutils wxwidgets

DESCRIPTION="GUI for abgx360"
HOMEPAGE="http://abgx360.net/"
SRC_URI="http://dl.dropbox.com/u/59058148/${P}.tar.gz"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="app-arch/abgx360
	x11-libs/wxGTK:2.8[X]"
DEPEND="${RDEPEND}"

# TODO: desktop file
src_install() {
	default
	make_desktop_entry abgx360gui abgx360
}
