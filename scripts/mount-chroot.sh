#!/bin/sh

# TODO:
# - dbus?
# - init?

if [ -z "$@" ]; then
	echo "missing operand" >&2
	exit 1
fi

if [ ! -d "$@"/dev ]; then
	echo "no valid chroot environment" >&2
	exit 1
fi

: ${PORTDIR="$(portageq portdir)"}
: ${DISTDIR="$(portageq envvar DISTDIR)"}
: ${PKGDIR="$(portageq envvar PKGDIR)"}

mount --rbind /dev "$@"/dev
mount --bind /sys "$@"/sys
mkdir -p "$@"/usr/portage
mount --bind "${PORTDIR}" "$@"/usr/portage
mkdir -p "$@"/{packages,distfiles}
mount --bind "${DISTDIR}" "$@"/distfiles
mount --bind "${PKGDIR}" "$@"/packages
mount -t proc none "$@"/proc
cp /etc/resolv.conf "$@"/etc

if [ -x "$@${SHELL}" ]; then
	exe=${SHELL}
else
	exe=/bin/bash
fi

chroot "$@" ${exe}
