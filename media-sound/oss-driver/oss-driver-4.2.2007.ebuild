# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
inherit linux-mod versionator

MY_PV=$(get_version_component_range 1-2)
BUILD=$(get_version_component_range 3)

DESCRIPTION="Open Sound System - drivers"
HOMEPAGE="http://developer.opensound.com/"
SRC_URI="mirror://debian/pool/main/o/oss4/oss4-dkms_${MY_PV}-build${BUILD}-2_amd64.deb"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_unpack() {
	unpack ${A}
	mkdir "${S}"
	cd "${S}"
	tar xf "${WORKDIR}"/data.tar.gz
}

src_prepare() {
	sed -e "s/-isystem \$(MULTIARCH_PATH)//" \
		-e "/^MULTIARCH_PATH/d" \
		-i usr/src/oss4-${MY_PV}-build${BUILD}/{core,drivers}/Makefile || die
}

src_compile() {
	cd usr/src/oss4-${MY_PV}-build${BUILD}
	set_arch_to_kernel
	cp /usr/include/linux/limits.h core
	emake -C "${KV_DIR}" SUBDIRS="$(pwd)"/core modules
	emake -C drivers osscore_symbols.inc
	emake -C "${KV_DIR}" SUBDIRS="$(pwd)"/drivers modules
}

src_install() {
	cd usr/src/oss4-${MY_PV}-build${BUILD}
	insinto /lib/modules/${KV_FULL}/kernel/sound/core
	doins core/osscore.${KV_OBJ}

	insinto /lib/modules/${KV_FULL}/kernel/sound/pci
	doins drivers/*.${KV_OBJ}
}

pkg_postinst() {
	elog "To get sound devices, you need to run"
	elog "	# ossdetect -d"
	elog "and"
	elog "	# ossdevlinks"
}
