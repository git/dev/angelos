# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
inherit toolchain-funcs user git-2

DESCRIPTION="ChaosVPN client"
HOMEPAGE="http://wiki.hamburg.ccc.de/index.php/ChaosVPN"
EGIT_REPO_URI="git://github.com/ryd/chaosvpn.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="dev-libs/openssl
	net-misc/tinc"

pkg_setup() {
	enewuser chaosvpn "" "" /etc/tinc/chaos/
}

src_prepare() {
	sed -i -e "/strip/d" Makefile || die
}

src_compile() {
	emake CC=$(tc-getCC)
}

src_install() {
	default

	diropts -m0700 -o chaosvpn -g chaosvpn
	dodir /etc/tinc/chaos/

	newinitd "${FILESDIR}/${PN}.init" ${PN}
}

pkg_postinst() {
	elog "Don't forget to run"
	elog "	# tincd -n chaos --generate-keys=2048"
	elog "if this is your first install."
}
