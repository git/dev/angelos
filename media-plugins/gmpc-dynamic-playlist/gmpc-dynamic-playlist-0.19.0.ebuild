# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
inherit eutils cmake-utils

DESCRIPTION="Makes a playlist with last.fm information"
HOMEPAGE="http://gmpc.wikia.com/wiki/GMPC_PLUGIN_DYNAMIC_PLAYLIST"
SRC_URI="https://bitbucket.org/misery/dynamic-playlist/downloads/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=media-sound/gmpc-${PV}"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	sys-devel/gettext"

src_prepare() {
	epatch "${FILESDIR}"/${P}-libmpd-0.20.patch
}
