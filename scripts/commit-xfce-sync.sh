#!/bin/sh

repo_name="xfce-dev"
[ $# -gt 0 ] && repo_name=$1
OVERLAY="$(portageq get_repo_path / ${repo_name})"
[ $? -eq 0 ] || exit 1

pushd "${OVERLAY}" >/dev/null
for i in $(git status --porcelain|grep ebuild|awk '{ print $2 }'); do
	pkgs+=" $(dirname ${i})"
done

for i in ${pkgs}; do
	pushd ${i} >/dev/null
	repoman ci -m "$(basename ${i}): Sync with gentoo-x86"
	popd >/dev/null
done
popd >/dev/null

echo "Everything committed, don't forget to push!"
