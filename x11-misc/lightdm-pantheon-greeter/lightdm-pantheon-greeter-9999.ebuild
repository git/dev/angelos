# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
VALA_API_MIN_VERSION=0.16

inherit cmake-utils bzr vala

DESCRIPTION="Pantheon Styled Login Screen for LightDM"
HOMEPAGE="https://launchpad.net/pantheon-greeter"
EBZR_REPO_URI="lp:pantheon-greeter"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
dev-libs/granite
x11-libs/gtk+:3
dev-libs/libindicator:3
media-libs/clutter-gtk
x11-misc/lightdm
"
DEPEND="${DEPEND}
	$(vala_depend)
"

src_prepare() {
	vala_src_prepare
	sed -e "/NAMES/s:valac:valac-$(vala_best_api_version):" \
		-i cmake/FindVala.cmake || die
}
