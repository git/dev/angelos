#!/bin/bash

repo_name="xfce-dev"
[[ $# -gt 0 ]] && repo_name=$1
OVERLAY="$(portageq get_repo_path / ${repo_name})"
[[ $? -eq 0 ]] || exit 1
PORTDIR="$(portageq get_repo_path / gentoo)"

# collect ebuilds
pushd "${OVERLAY}" >/dev/null
pkgs=$(echo */*/*.ebuild)
popd >/dev/null

changes=0

for i in ${pkgs}; do
	# get cat/pkg
	atom=( $(qatom ${i/\/*\//\/}) )
	CAT=${atom[0]}
	PN=${atom[1]}

	# get source ebuild's header
	declare -a src
	src=( ${PORTDIR}/${CAT}/${PN}/${PN}-*.ebuild )
	src=${src[-1]}
	srcheader=$(grep "^# \$Header: " ${src})

	# get destination ebuild's header
	dst=${OVERLAY}/${i}
	[[ -e ${dst} ]] || continue
	dstheader=$(grep "^# \$Header: " ${dst})

	if [[ "${srcheader}" != "${dstheader}" ]]; then
		# copy new ebuild
		cp -f ${src} ${dst}

		# remove any keywords
		ekeyword ^all ${dst} >/dev/null

		# remove SRC_URI
		sed -i -e "/^SRC_URI/d" ${dst}

		let changes="$changes + 1"
	fi
done

if [[ ${changes} -gt 0 ]]; then
	echo ${changes} ebuilds synced, please review the changes
else
	echo Everything up to date
fi
