# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

DESCRIPTION="OnlineTVRecorder decoder (QT)"
HOMEPAGE="http://www.onlinetvrecorder.com"
SRC_URI="http://onlinetvrecorder.com/downloads/${PN}-linux-Ubuntu_8.04-i686-${PV}-r1132.tar.bz2"

LICENSE="LGPL-2.1 MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="virtual/ffmpeg
	dev-qt/qtgui:4[abi_x86_32(-)]
	dev-qt/qtcore:4[abi_x86_32(-)]"
DEPEND=""

S=${WORKDIR}/${PN}-linux-Ubuntu_8.04-i686-${PV}-r1132

QA_PRESTRIPPED="usr/bin/qotr"
QA_DT_HASH="usr/bin/qotr"

src_install() {
	# TODO locales
	dobin qotr
}
