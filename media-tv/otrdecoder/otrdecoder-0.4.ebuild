# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=3

DESCRIPTION="OnlineTVRecorder decoding tool"
HOMEPAGE="http://www.onlinetvrecorder.com"
SRC_URI="x86? ( http://www.onlinetvrecorder.com/downloads/${PN}-bin-linux-Ubuntu_9.04-i686-${PV}.592.tar.bz2 )
	amd64? ( http://www.onlinetvrecorder.com/downloads/${PN}-bin-linux-Ubuntu_8.04.2-x86_64-${PV}.613.tar.bz2 )"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk"

RDEPEND="gtk? ( dev-python/pygtk )"
DEPEND=""

S=${WORKDIR}/${PN}-bin-linux-Ubuntu_8.04.2-x86_64-${PV}.613

QA_PRESTRIPPED="usr/bin/otrdecoder"
QA_DT_HASH="usr/bin/otrdecoder"

src_install() {
	dobin ${PN} || die "dobin failed"
	if use gtk; then
		dobin ${PN}-gui || die "dobin (GUI) failed"
	fi
	dodoc README.OTR || die "dodoc failed"
}
