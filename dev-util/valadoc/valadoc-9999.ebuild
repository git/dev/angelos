# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
VALA_MIN_API_VERSION=0.16
inherit autotools git-2 vala

DESCRIPTION="a documentation generator for Vala source code"
HOMEPAGE="https://live.gnome.org/Valadoc"
EGIT_REPO_URI="git://git.gnome.org/valadoc"
EGIT_BOOTSTRAP="eautoreconf"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="$(vala_depend)
	>=dev-libs/glib-2.12:2
	>=dev-libs/libgee-0.5:0
	>=media-gfx/graphviz-2.16
	x11-libs/gdk-pixbuf:2
	>=x11-libs/gtk+-2.10:2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

DOCS=( AUTHORS MAINTAINERS THANKS )

src_configure() {
	econf --disable-static
}

src_install() {
	default
	find "${ED}" -name "*.la" -type f -exec rm -rf {} + || die
}
