# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
VALA_MIN_API_VERSION=0.16
inherit autotools git-2 vala

DESCRIPTION="an IRC bot written in Vala with support for dynamic plugins and a sqlite3-based database"
HOMEPAGE="https://github.com/cmende/foobot-vala"
EGIT_REPO_URI="git://github.com/cmende/${PN}-vala.git"
EGIT_BOOTSTRAP="eautoreconf"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-libs/glib:2
	dev-libs/libpeas
	dev-libs/sqlheavy"
DEPEND="${RDEPEND}
	$(vala_depend)
	virtual/pkgconfig"

src_install() {
	default
	find "${ED}" -name "*.la" -delete || die
}
