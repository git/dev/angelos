# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
inherit eutils flag-o-matic toolchain-funcs versionator

MY_PV=$(get_version_component_range 1-2)
BUILD=$(get_version_component_range 3)
MY_P=${PN}-v${MY_PV}-build${BUILD}-src-gpl

DESCRIPTION="Open Sound System - applications and man pages"
HOMEPAGE="http://developer.opensound.com/"
SRC_URI="http://www.4front-tech.com/developer/sources/stable/gpl/${MY_P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk salsa"

RDEPEND="media-sound/oss-driver
	gtk? ( x11-libs/gtk+:2 )"
DEPEND="${RDEPEND}
	app-text/txt2man"

S=${WORKDIR}/${MY_P}
BUILD_DIR=${WORKDIR}/${PN}-build

### TODO ###
# - fix linking order for --as-needed
# - fix man pages (savemixer installed twice)

src_prepare() {
	# the build system forces shadow builds
	mkdir "${BUILD_DIR}"
	epatch "${FILESDIR}"/${P}-filesystem-layout.patch \
		"${FILESDIR}"/${P}-txt2man.patch \
		"${FILESDIR}"/${P}-as-needed-strip.patch
	sed -e "s:GRC_MAX_QUALITY=3:GRC_MAX_QUALITY=6:" -i configure || die
}

src_configure() {
	cd "${BUILD_DIR}"

	local myconf=""

	use salsa || myconf="--enable-libsalsa=NO"

	HOSTCC=$(tc-getCC) \
	NO_WARNING_CHECKS=1 \
	"${S}"/configure \
		--config-midi=YES \
		${myconf} || die
}

src_compile() {
	cd "${BUILD_DIR}"

	pushd lib
	einfo "Building libraries"
	emake CC=$(tc-getCC)
	popd

	pushd cmd
	if ! use gtk; then
		# remove ossxmix from SUBDIRS
		sed -e "s:ossxmix::" -i Makefile
	fi

	einfo "Building applications"
	emake CC=$(tc-getCC)
	popd

	pushd os_cmd/Linux
	einfo "Building ossdetect/ossvermagic"
	emake CC=$(tc-getCC)
	popd
}

src_install() {
	cd "${BUILD_DIR}"
	use salsa && dolib lib/libsalsa/.libs/libsalsa.so*

	dolib lib/libOSSlib/libOSSlib.so

	# linux-headers ships OSS3 API
	#insinto /usr/include/linux
	#doins include/soundcard.h

	# install man pages
	use gtk || rm cmd/ossxmix/ossxmix.man
	rename man 1 cmd/*/*.man
	doman cmd/*/*.1
	rename .man .7 misc/man7/*.man
	doman misc/man7/*.7
	rename man 7 kernel/drv/*/*.man
	doman kernel/drv/*/*.7
	newman os_cmd/Linux/ossdetect/ossdetect.man ossdetect.8
	newman noregparm/cmd/ossdevlinks/ossdevlinks.man ossdevlinks.8
	newman noregparm/cmd/savemixer/savemixer.man savemixer.8
	newman noregparm/cmd/vmixctl/vmixctl.man vmixctl.8

	insinto /etc/oss4
	doins devices.list
	newins .version version.dat
	cat > "${ED}"/etc/oss.conf << EOF
OSSETCDIR=/etc/oss4
OSSVARDIR=/var/lib/oss4
EOF

	cd "target"
	dosbin sbin/*
	dobin bin/*
	dolib lib/*

	dodir /var/lib/oss4

	newinitd "${FILESDIR}"/${PN}.init ${PN}
	newconfd "${FILESDIR}"/${PN}.conf ${PN}
}
