# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
VALA_MIN_API_VERSION=0.16
inherit autotools git-2 vala

DESCRIPTION="GObject SQLite wrapper"
HOMEPAGE="https://github.com/nemequ/sqlheavy"
EGIT_REPO_URI="https://github.com/nemequ/${PN}.git"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS=""
IUSE="doc examples"

RDEPEND=">=dev-db/sqlite-3.6.20:3
	$(vala_depend)
	>=dev-libs/glib-2.22:2
	>=x11-libs/gtk+-2.24:2"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? ( dev-util/valadoc )"

DOCS=( AUTHORS NEWS README )

src_prepare() {
	sed -i -e "/examples/d" Makefile.am # don't compile examples
	eautoreconf
	vala_src_prepare
}

src_configure() {
	econf \
		--disable-static \
		$(use_enable doc valadoc)
}

src_install() {
	default
	find "${ED}" -name "*.la" -delete || die
	docinto examples
	use examples && dodoc examples/*.vala
}
