# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit autotools git-r3

DESCRIPTION="Wifi-Display/Miracast Implementation"
HOMEPAGE="http://www.freedesktop.org/wiki/Software/miracle/"
EGIT_REPO_URI="git://people.freedesktop.org/~dvdhrm/miracle"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND=">=dev-libs/glib-2.38:2
	net-libs/libwfd
	>=sys-apps/systemd-211[kdbus]"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	eautoreconf
}
