# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
PYTHON_COMPAT=( python2_7 python3_2 )

inherit eutils fdo-mime python-r1 distutils-r1 git-2

DESCRIPTION="A small GTK tool to connect to VNC servers"
HOMEPAGE="https://cmende.github.com/gtkvncviewer"
EGIT_REPO_URI="git://github.com/cmende/${PN}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-libs/gobject-introspection
	gnome-base/libgnome-keyring[introspection]
	net-libs/gtk-vnc[gtk3,introspection]
	x11-libs/gdk-pixbuf:2[introspection]
	x11-libs/gtk+:3[introspection]"
DEPEND="${RDEPEND}"

pkg_postinst() { fdo-mime_desktop_database_update; }
pkg_postrm() { fdo-mime_desktop_database_update; }
