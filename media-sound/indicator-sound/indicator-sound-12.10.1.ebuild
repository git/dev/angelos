# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4
VALA_MIN_API_VERSION=0.14
inherit gnome2-utils vala

DESCRIPTION="A unified sound menu"
HOMEPAGE="https://launchpad.net/indicator-sound"
SRC_URI="http://launchpad.net/${PN}/12.10/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=dev-libs/glib-2.26:2
	>=dev-libs/libdbusmenu-0.5.90[gtk]
	dev-libs/libgee
	dev-libs/libindicator:3
	dev-libs/libxml2:2
	media-sound/pulseaudio[glib]
	x11-libs/gtk+:3
	x11-libs/libido
	>=x11-libs/libnotify-0.7"
DEPEND="${RDEPEND}
	$(vala_depend)
	virtual/pkgconfig"

DOCS=( AUTHORS NEWS )

src_configure() {
	econf \
		--disable-static \
		--disable-schemas-compile
}

src_install() {
	default
	find "${ED}" -name "*.la" -exec rm -rf {} +
}

pkg_preinst() { gnome2_schemas_savelist; }
pkg_postinst() { gnome2_schemas_update; }
pkg_postrm() { gnome2_schemas_update; }
