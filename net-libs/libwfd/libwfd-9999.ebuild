# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit autotools git-r3

DESCRIPTION="a stand-alone implementation of the Wifi-Display protocol"
HOMEPAGE="http://www.freedesktop.org/wiki/Software/miracle/libwfd/"
EGIT_REPO_URI="git://people.freedesktop.org/~dvdhrm/${PN}"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

src_prepare() {
	eautoreconf
}
