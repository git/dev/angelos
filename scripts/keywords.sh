#!/bin/sh

# TODO:
# - support multiple arches with one call (auto-inject arch only if none was given, obsoletes "stable" symlink)
# - parse atoms to void having to cd into PORTDIR/foo/bar
# - support multiple packaegs with one call (depends on ^)
# - support pybugz to remove arches from CC

PKG_VERSION=0.1
PKG_NAME=gkeywords

BIN="$(basename $0)"

if [ "${BIN}" == "gkw" ]; then
	echo "Don't call this script directly, dumbass!" >&2
	exit 1
fi

print_usage() {
	echo "Usage:"
	echo "  ${BIN} [OPTION...]"
	echo
	echo "Help Options:"
	echo "  -h, --help	Show help options"
	echo
	echo "Application Options:"
	echo "  -v, --version	Print the program version"
	echo
	exit 0
}

print_version() {
	echo "${PKG_NAME} ${PKG_VERSION}"
	echo "Copyright 2011 Christoph Mende <angelos@gentoo.org>"
	exit 0
}

while getopts hv opt; do
	case "$opt" in
	h) print_usage ;;
	v) print_version ;;
	\?) print_usage ;;
	esac
done

ebuild=$1
bug=$2

if ! [ -f "${ebuild}" ]; then
	echo "ebuild not found" >&2
	exit 1
fi

case $(uname -m) in
	x86_64) ARCH="amd64";;
	?) ARCH="$(uname -m)";;
esac

case ${BIN} in
	stable)
		string="Stable on ${ARCH}"
		keyword="${ARCH}"
		;;
	keyword)
		string="Add ~${ARCH}"
		keyword="~${ARCH}"
		;;
	?)
		echo "wat" >&2
		exit 1
		;;
esac

if [ -n "${bug}" ]; then
	string+=" wrt bug #${bug}"
fi

if ! ekeyword ${keyword} ${ebuild}; then
	echo "ekeyword failed" >&2
	exit 1
fi

if ! echangelog "${string}"; then
	echo "echangelog failed" >&2
	exit 1
fi

if ! repoman ci -m "${string}"; then
	echo "repoman failed" >&2
	exit 1
fi
